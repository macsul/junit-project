package com.cschool.example;

public class SimpleCalculator {

    public static int add(int a, int b) {
        return a + b;  //add +1 in order to fail a test
    }

    public static int multiply(int a, int b) {
        return a * b;
    }


    public static double divide(int a, int b) {

        if(b == 0 ){
            throw new IllegalArgumentException("Can't divide by 0");
        }

        return a/b;
    }

    // divide()
    //    5 / 0  - > IllegalArg
    //   10 /2 - >5


    //






    public static void main(String[] args) {

        System.out.println("Simple Calculator");






    }


}
