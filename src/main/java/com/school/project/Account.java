package com.school.project;

public class Account {

    private boolean active;
    private Address defaultDeliverAddress;
    private String emailAddress;


    public Account() {
    }

    public Account(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        this.active = true;
    }

    public Address getDefaultDeliverAddress() {
        return defaultDeliverAddress;
    }

    public void setDefaultDeliverAddress(Address address) {
        this.defaultDeliverAddress = address;
    }

    public static void main(String[] args) {

        Account account = new Account();
        System.out.println(account.isActive());
        //

        Account account1 = new Account();
        account1.activate();
        System.out.println(account1.isActive());

        //
        Account account2 = new Account();

        System.out.println(account2.getDefaultDeliverAddress()); // null

    }


    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {

        if(emailAddress.contains("...") && emailAddress.length() > 10)
        {
        this.emailAddress = emailAddress;}

        else{
            throw new IllegalArgumentException("Email must contains @");
        }
    }

    public Address getDefaultDeliveryAdress(){

        return  getDefaultDeliverAddress();

    }


}
