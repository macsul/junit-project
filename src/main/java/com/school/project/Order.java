package com.school.project;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private List<Meal> meals = new ArrayList<>();

    void addMealToOrder(Meal mealToAdd){
        meals.add(mealToAdd);


    }

    void removeMealFromOrder(Meal mealToRemove){
        meals.remove(mealToRemove);


    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void addMealToOrder(List<Meal> mealList1) {

        meals.addAll(mealList1);
    }
}


