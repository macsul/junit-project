package com.school.project;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void newlyCreatedAccountShouldNotBeActive(){







       // given
        Account account = new Account();

        //then
        assertFalse(account.isActive(),
                "Check if new account is not active");

      //  assertThat(account.isActive(), equalTo(false);
      //  assertThat(account.isActive(), is(false));

    }


    @Test
    void newlyCreatedAccountShouldBeActiveAfterActivation(){
        //given
        Account account = new Account();

        //when
        account.activate();

        //then
        assertTrue(account.isActive(),
                "Check if new account is not active");

    }

    @Test
    void newlyCreatedAccountShouldHaveDefaultDeliveryAddressEqualToNull(){





        //1
//        //given
//        Account account = new Account();
//        //when
//        Address address = account.getDefaultDeliverAddress();
//        //then
//        assertNull(address);

        // 2
        //given
        Account account1 = new Account();

        //then
        assertNull(account1.getDefaultDeliverAddress());



    }

    @Test
    void deliveryAddressShouldNotBeNullAfterBeingSetInAccount(){

        // 2
        //given
        Account account1 = new Account();

        //when
        account1.setDefaultDeliverAddress(new Address("Szkolna", "100a"));

        //then
        assertNotNull(account1.getDefaultDeliverAddress());
        assertThat(account1.getDefaultDeliverAddress(), notNullValue());
    }


    @Test

    void correctEmail(){
        //given
        Account account1 = new Account();

        //when
        account1.setEmailAddress("james@gmail.com");

        //then

        assertThat(account1.getEmailAddress(),notNullValue());



    }

    @Test

    void givenWrongEmail(){

        Account account1 = new Account();

        Assertions.assertThrows(IllegalArgumentException.class, () ->
            account1.setEmailAddress("james@gmail.com"));
                };



        /*

         @Test
    public void method2(){

        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            account1.set
        });
         */
    }


    @ValueSource(strings = {"james@gmail.com", "thomas@gmail.com", "admin@wp.pl"})
    @ParameterizedTest

    void correctEmailAddress(String str){

        Account account = new Account();


        account.setEmailAddress(str);

        assertThat(account.getEmailAddress(),notNullValue());



    }

    @Test
    void  givenToolShortEmailAddressShouldThrowException(){

        Account account = new Account();

      //  account.setEmailAddress("12345678910");
       // account.setEmailAddress(("123456789"));

      //  assertThat("12345", hasLength(2));
        Assertions.assertThrows(IllegalArgumentException.class, () -> account.setEmailAddress("james@gmail.com"));

    }





}