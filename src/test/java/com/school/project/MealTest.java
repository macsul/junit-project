package com.school.project;

import com.sun.xml.internal.ws.api.model.MEP;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class MealTest {

    @Test
    void shouldReturnDiscountedPrice(){
        //given
        Meal meal = new Meal(10);

        // when
        int priceAfterDiscount = meal.getDiscountPrice(4);

        //then
        assertEquals(6, priceAfterDiscount);

    }


    @Test
    void discountGreaterThanPriceShouldThrowException(){
        //given
        Meal meal = new Meal(10);

        //when
        Executable lambda = () -> meal.getDiscountPrice(12);

        //then
        //1
//        assertThrows(IllegalArgumentException.class, () ->{
//            meal.getDiscountPrice(2);
//        });

        //2
        assertThrows(IllegalArgumentException.class, lambda );

    }

    @Test
    void zadDom(){
        //given
        Meal meal = new Meal(10);
        Meal meal2 = meal;

        //when, then
        assertSame(meal, meal2);
    }


    @Test
    void referencesToDifferentObjectShouldNotBeEqual(){
        //given
        Meal meal = new Meal(10);
        Meal meal2 = new Meal(20);

        //when, then
        assertNotSame(meal, meal2);
    }

    //

    @Test
    void twoMealsShouldBeEqualWhenPriceAndNameAreTheSame(){
        //given
        Meal meal = new Meal(10,"Burger");
        Meal meal2 = new Meal(10,"Burger");

        //when, then
        assertEquals(meal, meal2);
    }








}