package com.school.project;

import com.sun.org.apache.xpath.internal.operations.Or;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.OptionalInt.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    void testAssertArrayEquals(){

        int[] ints1 = {1,2,3};
        int[] ints2 = {1,2,3};

        //then

        assertArrayEquals(ints1,ints2);

    }

    @Test

    void mealListShouldBeEmptyAfterCreationOfOrder (){
        Order order = new Order();

        assertThat(order.getMeals(), empty());
        assertThat(order.getMeals().size(), equalTo());
        assertThat(order.getMeals(), hasSize(0));
        assertThat(order.getMeals(),emptyCollectionOf(Meal.class));
    }


    @Test

    void addingMeal(){

        Meal meal = new Meal (5, "KEBAB");
        Order order = new Order();

        order.addMealToOrder(meal);



        // assertThat(meal.getMeals(), hasSize(1));

    }

    @Test

    addingMealToOrderShouldIncreaseOrderListSize (){

        Meal meal = new Meal(10, "Burger");
        Meal meal2 = new Meal(12, "Pizza");

        Order order = new Order();
        order.addMealToOrder(meal);

        assertThat(order.getMeals(), contains((meal)));
        assertThat(order.getMeals(), hasItem(meal));

        assertThat(order.getMeals().get(0).getPrice(),equalTo(10));


    }

    @Test

    removingMealFromOrderShouldDecreaseOrderSize (){

        Meal meal = new Meal(10, "Burger");
        Meal meal2 = new Meal(12, "Pizza");

        Order order = new Order();
        order.addMealToOrder(meal);
        order.removeMealFromOrder(meal);

        assertThat(order.getMeals(), not (hasItem(meal)));


    }

    @Test
    void mealsShouldBeInCorrectOrderAfterAddingThemToOrder (){

        Meal meal = new Meal(10, "Burger");
        Meal meal2 = new Meal(12, "Pizza");

        Order order = new Order();

        order.addMealToOrder(meal);
        order.addMealToOrder(meal2);

        assertThat(order.getMeals(),contains(meal,meal2));

        assertThat(order.getMeals(),containsInAnyOrder(meal,meal2));

        assertThat(order.getMeals(),hasItems(meal,meal2));

        assertThat(order.getMeals(),hasItem(meal));

    }

    @Test
    void testIfTwoOrdersAreTheSame(){
        Meal meal = new Meal(10, "Burger");
        Meal meal2 = new Meal(12, "Pizza");
        Meal meal3 = new Meal(22, "Pierogi");
        List<Meal> mealList1 = Arrays.asList(meal, meal2);
        List<Meal> mealList2 = Arrays.asList(meal, meal2);
        List<Meal> mealList3 = Arrays.asList(meal2, meal);
        List<Meal> mealList4 = Arrays.asList(meal2, meal, meal3);
        Order order1 = new Order();
        Order order2 = new Order();
        order1.addMealToOrder(mealList1);
        order2.addMealToOrder(mealList2);
        assertThat(order1.getMeals(), is(order2.getMeals()));
    }
}

